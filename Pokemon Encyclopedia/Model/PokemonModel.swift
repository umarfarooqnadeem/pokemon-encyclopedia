//
//  PokemonModel.swift
//  Pokemon Encyclopedia
//
//  Created by Umar Farooq on 18/07/2021.
//

import Foundation

struct PokemonModel: Codable {
    let name      : String?
    var detail    : PokemonDetailModel?
    var imageData : Data?
    
    init(data: [String: Any]) {
        name       = data["name"] as? String
        detail     = PokemonDetailModel(data: data)
        imageData  = nil
    }
}

struct PokemonDetailModel: Codable {
    var abilities : [String]? = []
    let number    : Int?
    let image     : String?
    let height    : Int?
    let weight    : Int?
    let baseXP    : Int?
    
    init(data: [String: Any]) {
        if let abilities = data["abilities"] as? [[String: Any]] {
            self.abilities = abilities.enumerated().map { index, result -> String in
                if index < 2 {
                    if let ability = result["ability"] as? [String: Any]{
                        if let name = ability["name"] as? String {
                            return name
                        }
                    }
                }
                return ""
            }
        }
          
        
        number = data["order"] as? Int
        
        if let images = data["sprites"] as? [String: Any] {
            if let other = images["other"] as? [String: Any] {
                if let officialArtwork = other["official-artwork"] as? [String: Any] {
                    image = officialArtwork["front_default"] as? String
                } else {
                    image = ""
                }
            } else {
                image = ""
            }
        } else {
            image = ""
        }
        
        height = data["height"] as? Int
        weight = data["weight"] as? Int
        baseXP = data["base_experience"] as? Int
    }
}
