//
//  DetailsVC.swift
//  Pokemon Encyclopedia
//
//  Created by Umar Farooq on 20/07/2021.
//

import UIKit

class DetailsVC: UIViewController {

    /* MARK:- Properties */
    private var detailsView: DetailsView = DetailsView()
    public var pokemonDatum: PokemonModel?
    
    /* MARK:- Life Cycle */
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func loadView() {
        super.loadView()
        view = detailsView
        if let pokemonDatum = pokemonDatum {
            detailsView.pokemonDatum = pokemonDatum
        }
    }
    
    deinit {
        Helper.debugLogs(anyData: "DEINIT", andTitle: "SUCCESS")
    }
}
