//
//  ViewController.swift
//  Pokemon Encyclopedia
//
//  Created by Umar Farooq on 18/07/2021.
//

import UIKit

class HomeVC: UIViewController {
    /* MARK:- Properties */
    ///Private
    private let group          : DispatchGroup   = DispatchGroup()
    private var tempData       : [PokemonModel?] = []
    private var isFetchingData : Bool            = false
    ///Public
    public let homeView     : HomeView        = HomeView()
    public var originalData : [PokemonModel]  = []
    public var filteredData : [PokemonModel]  = []
    public var isSearching  : Bool            = false
    
    public var count : Int  = 1
    
    /* MARK:- Life Cycle */
    override func viewDidLoad() {
        super.viewDidLoad()
        initVC()
    }
    
    override func loadView() {
        super.loadView()
        view = homeView
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        tempData = []
    }
}

/* MARK:- Methods */
extension HomeVC {
    func initVC(){
        tableViewSetup()
        
        if let pokemonData = POKEMON_DATA {
            if let sortSelected = SortTypes(rawValue: SORT_SELECTED) {
                Helper.sort(modelData: pokemonData, byType: sortSelected) { [weak self] result in
                    guard let self = self else {return}
                    self.originalData = result
                    self.filteredData = result
                    POKEMON_DATA      = self.originalData
                    self.homeView.tableView.reloadData()
                }
            }
        } else {
            fetchPokemon()
        }
        
        self.showToast(message: "Request timeout")
    }
    
    func tableViewSetup(){
        homeView.tableView.delegate   = self
        homeView.tableView.dataSource = self
        
        homeView.tableView.register(
            PokemonCell.self,
            forCellReuseIdentifier: Constants.shared.PokemonCell
        )
    }
}

/* MARK:- TableView */

///DATASOURCE
extension HomeVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: Constants.shared.PokemonCell
        ) as! PokemonCell
                
        autoreleasepool {
            let row = indexPath.row
            cell.configure(withData: filteredData[row], atIndex: indexPath.row)
        }
        
        return cell
    }
}

///DATASOURCE
extension HomeVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        
        let detailsVC = DetailsVC()
        
        detailsVC.pokemonDatum = filteredData[row]
        
        navigationController?.pushViewController(detailsVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ((SCREEN_HEIGHT * 0.9) / 4.0)
    }
}

/* MARK:- Scroll View */

///Delegate
extension HomeVC : UIScrollViewDelegate{
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView == homeView.tableView {
            if !isFetchingData, !isSearching {
                if originalData.count < 300 {
                    if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
                        OFFSET += 10
                        isFetchingData = true
                        
                        homeView.tableView.tableFooterView = Helper.showLoadingFooter(
                            onView: homeView.tableView
                        )
                        self.fetchPokemon(withLoader: false)
                    }
                }
            }
        }
    }
}

/* MARK:- API Methods */
extension HomeVC {
    func fetchPokemon(withLoader loader: Bool = true){
        if loader {
            showSpinner(onView: view)
        }
        
        NetworkManager.shared.makeRequest(
            toUrl: "https://pokeapi.co/api/v2/pokemon/?offset=\(OFFSET)&limit=\(RESULT_LIMIT)"
        ) { [weak self] response in
            guard let self = self else { return }
            
            switch response {
            case .success(let result):
                
                if let data = result["results"] as? [[String: Any]] {
                    for datum in data {
                        if let url = datum["url"] as? String {
                            self.group.enter()
                            self.fetchPokemonDetails(fromUrl: url)
                        }
                    }
                    
                    self.group.notify(queue: .main) { [weak self] in
                        guard let self = self else {return}
                        self.removeSpinner()
                        self.isFetchingData = false
                        if self.tempData.count == RESULT_LIMIT {
                            for datum in self.tempData {
                                if let pokemonDatum = datum {
                                    self.originalData.append(pokemonDatum)
                                }
                            }
                            
                            self.tempData = []
                            
                            if let sortSelected = SortTypes(rawValue: SORT_SELECTED) {
                                Helper.sort(
                                    modelData: self.originalData,
                                    byType   : sortSelected
                                ) { [weak self] result in
                                    guard let self = self else {return}
                                    
                                    self.originalData = result
                                    self.filteredData = result
                                    POKEMON_DATA      = result
                                    self.homeView.tableView.tableFooterView = nil
                                    self.homeView.tableView.reloadData()
                                }
                            }
                        }
                    }
                }
                
                Helper.debugLogs(anyData: self.originalData, andTitle: "SUCCESS")
            case .badCode(let code):
                self.removeSpinner()
                Helper.debugLogs(anyData: code, andTitle: "BAD CODE")
            case .invalid(let message):
                self.removeSpinner()
                Helper.debugLogs(anyData: message, andTitle: "INVALID")
            case .timeout(let message):
                self.removeSpinner()
                Helper.debugLogs(anyData: message, andTitle: "TIME OUT")
            case .faliure(let error):
                self.removeSpinner()
                Helper.debugLogs(anyData: error, andTitle: "FALIURE")
            }
        }
    }
    
    func fetchPokemonDetails(fromUrl url: String) {
        NetworkManager.shared.makeRequest(
            toUrl: url
        ) { [weak self] response in
            guard let self = self else { return }
            
            Helper.debugLogs(anyData: self.count, andTitle: "CALLED")
            
            switch response {
            case .success(let result):
                let datum = PokemonModel(data: result)
                                
                MAIN_QUEUE.async {
                    if let image = datum.detail?.image {
                        if let url = URL(string: image) {
                            NetworkManager.shared.loadData(
                                url: url,
                                withIndicatorOnImageView: nil
                            ) { data, url, error in
                                self.tempData.append(datum)
                                self.group.leave()
                            }
                        }
                    }
                }
                                                
//                Helper.debugLogs(anyData: result, andTitle: "SUCCESS")
            case .badCode(let code):
                self.group.leave()
                self.removeSpinner()
                self.tempData.append(nil)
                Helper.debugLogs(anyData: code, andTitle: "BAD CODE")
            case .invalid(let message):
                self.group.leave()
                self.removeSpinner()
                self.tempData.append(nil)
                Helper.debugLogs(anyData: message, andTitle: "INVALID")
            case .timeout(let message):
                self.group.leave()
                self.removeSpinner()
                self.tempData.append(nil)
                Helper.debugLogs(anyData: message, andTitle: "TIME OUT")
            case .faliure(let error):
                self.group.leave()
                self.removeSpinner()
                self.tempData.append(nil)
                Helper.debugLogs(anyData: error, andTitle: "FALIURE")
            }
                        
            self.count += 1
        }
        
    }
}
