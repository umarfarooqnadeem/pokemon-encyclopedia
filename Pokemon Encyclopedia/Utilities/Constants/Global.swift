//
//  Global.swift
//  Pokemon Encyclopedia
//
//  Created by Umar Farooq on 18/07/2021.
//

import UIKit

/* MARK:- Constants */
var SPINNER           : [UIView] = []
let APP_NAME          = "Pokemon Encyclopedia"
let DEFAULTS          = UserDefaults.standard
let MAIN_QUEUE        = DispatchQueue.main
let BG_QUEUE          = DispatchQueue.global(qos: .background)
let APPDELEGATE       = UIApplication.shared.delegate as! AppDelegate
let SCENEDELEGATE     = SceneDelegate.shared
let SCREEN_WIDTH      = UIScreen.main.bounds.width
let SCREEN_HEIGHT     = UIScreen.main.bounds.height
let RESULT_LIMIT      = 10

/* MARK:- Application Enum */
enum CurrentDeviceType {
    case iPad
    case bezelLessiPhone
    case bezeliPhone
}

enum NetworkResponse {
    case success([String: Any])
    case badCode(Int)
    case invalid(String)
    case timeout(String)
    case faliure(Error)
}

enum SortTypes: String {
    case alphabetically = "aplhabetically"
    case numerically    = "numerically"
}
