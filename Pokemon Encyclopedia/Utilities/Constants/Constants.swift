//
//  Constants.swift
//  Pokemon Encyclopedia
//
//  Created by Umar Farooq on 18/07/2021.
//

import Foundation

struct Constants {
    static let shared: Constants = Constants()
    
    ///TableView Cells
    let PokemonCell = "pokemonCell"
    
    ///User Defaults
    let PokemonData = "pokemonData"
    let Offset      = "offset"
    let SortBy      = "sortBy"
}

var SESSION: URLSession = {
    let configuration: URLSessionConfiguration = .default
    configuration.requestCachePolicy           = .returnCacheDataElseLoad
    configuration.timeoutIntervalForRequest    = 10.0
    configuration.timeoutIntervalForResource   = 10.0
    
    return URLSession(configuration: .default)
}()

var POKEMON_DATA: [PokemonModel]? {
    get {
        if let data = DEFAULTS.object(forKey: Constants.shared.PokemonData) as? Data {
            let decoder = JSONDecoder()
            if let pokemonData = try? decoder.decode([PokemonModel].self, from: data) {
               return pokemonData
            }
        }
        
        return nil
    } set {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(newValue) {
            DEFAULTS.set(encoded, forKey: Constants.shared.PokemonData)
        }
    }
}

var OFFSET: Int {
    get {
        return DEFAULTS.integer(forKey: Constants.shared.Offset)
    } set {
        DEFAULTS.setValue(newValue, forKey: Constants.shared.Offset)
    }
}

var SORT_SELECTED: String {
    get {
        return DEFAULTS.string(forKey: Constants.shared.SortBy) ?? "aplhabetically"
    } set {
        DEFAULTS.setValue(newValue, forKey: Constants.shared.SortBy)
    }
}
