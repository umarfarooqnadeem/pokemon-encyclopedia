//
//  UIView+Ext.swift
//  Pokemon Encyclopedia
//
//  Created by Umar Farooq on 19/07/2021.
//

import UIKit

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder?.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}
