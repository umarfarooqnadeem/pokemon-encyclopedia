//
//  ShadowNCornerRadiusView.swift
//  Pokemon Encyclopedia
//
//  Created by Umar Farooq on 20/07/2021.
//

import UIKit

class ShadowNCornerRadiusView: UIView {
    /* MARK:- Initilizers  */
    override init(frame: CGRect) {
        super.init(frame: frame)
        createView()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

extension ShadowNCornerRadiusView {
    func createView() {
        layer.cornerRadius = 10
        
        layer.shadowColor   = UIColor.black.cgColor
        layer.shadowOffset  = CGSize(width: 0, height: 2)
        layer.shadowOpacity = 0.6
        layer.shadowRadius  = 4.0
    }
}
