//
//  CustomLabel.swift
//  Pokemon Encyclopedia
//
//  Created by Umar Farooq on 20/07/2021.
//

import UIKit

class CustomLabel: UILabel {
    /* MARK:- Properties  */
    private var withFont: UIFont
    
    /* MARK:- Initilizers  */
    init(withFont font: UIFont) {
        withFont = font
        super.init(frame: .zero)
        createView()
    }
    override init(frame: CGRect) {
        withFont = UIFont.systemFont(ofSize: 14)
        super.init(frame: frame)
        createView()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

extension CustomLabel {
    func createView() {
        font          = withFont
        textColor     = UIColor.AppTheme.blue
        textAlignment = .center
    }
}
