//
//  Protocols.swift
//  Pokemon Encyclopedia
//
//  Created by Umar Farooq on 18/07/2021.
//

import Foundation

extension StringProtocol {
    var firstUppercased: String { return prefix(1).uppercased() + dropFirst() }
}
