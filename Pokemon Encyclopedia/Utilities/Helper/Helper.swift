//
//  Helper.swift
//  Pokemon Encyclopedia
//
//  Created by Umar Farooq on 18/07/2021.
//

import UIKit

class Helper {
    
    /// - Parameters:
    ///   - data: The data to be printed
    ///   - title: Title of what is to be printed
    static func debugLogs(anyData data: Any, andTitle title: String = "Log") {
        print("============= DEBUG LOGS START =================")
        print("\(title): \(data)")
        print("=============  DEBUG LOGS END  =================")
        print("\n \n")
    }
    
    /// Setup the initial VC of our application
    /// - Parameter window: our application windows
    static func setInitialViewController(
        withScene scene: UIWindowScene? = nil
    ){
        let window: UIWindow
        let rootViewController   = HomeVC()
        let navigationController = UINavigationController(rootViewController: rootViewController)
        navigationController.navigationBar.isHidden = true
        
        if #available(iOS 13.0, *) {
            if let windowScene = scene {
                window = UIWindow(windowScene: windowScene)
                ///Forcing Light Mode
                window.overrideUserInterfaceStyle = .light
                window.rootViewController = navigationController /// Initial VC
                window.makeKeyAndVisible()
                SCENEDELEGATE?.window = window
            }
        } else {
            window = UIWindow(frame: UIScreen.main.bounds)
            window.rootViewController = navigationController /// Initial VC
            window.makeKeyAndVisible()
            APPDELEGATE.window = window
        }
         
    }
    
    /// Method to sort pokemon data on basis of type
    /// - Parameters:
    ///   - data: the data to be sorted
    ///   - type: type of sort
    ///   - completion: completion handler
    static func sort(
        modelData data : [PokemonModel] ,
        byType type    : SortTypes      ,
        completion     : @escaping (_ completeed: [PokemonModel]) -> ()
    ) {
        var response : [PokemonModel] = []
        
        switch type {
        case .alphabetically:
            response = data.sorted { datum1, datum2 in
                if let name1 = datum1.name,
                   let name2 = datum2.name {
                    return name1 < name2
                }
                return false
            }
            
            completion(response)
        case .numerically:
            response = data.sorted { datum1, datum2 in
                if let number1 = datum1.detail?.number,
                   let number2 = datum2.detail?.number {
                    return number1 < number2
                }
                return false
            }
            
            completion(response)
        }
    }
    
    static func search(text searchText: String, fromData data: [PokemonModel]) -> [PokemonModel] {
        return data.filter({
            $0.name != nil && $0.name!.lowercased().contains(searchText.lowercased()) ||
            $0.detail?.number != nil && $0.detail!.number! == Int(searchText) ||
                $0.detail?.abilities != nil && $0.detail!.abilities!.contains(where: { $0.contains(searchText.lowercased())
                })
        })
    }
    
    /// Method to add Linear Gradient
    /// - Parameters:
    ///   - view: view on which gradient is to be added
    ///   - colors: gradient colors
    ///   - locations: location point at which gradient should be added
    /// - Returns: gradient layer
    static func getGradientLayer(
        inView view           : UIView    ,
        withColors colors     : [CGColor] ,
        atLocations locations : [NSNumber]) -> CAGradientLayer {
        
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        
        gradientLayer.frame     = view.bounds
        gradientLayer.colors    = colors
        gradientLayer.locations = locations
        
        return gradientLayer
    }
    
    static func showLoadingFooter(
        onView view     : UIView,
        withColor color : UIColor = UIColor.AppTheme.yellow) -> UIView {
        
        let footerView = UIView(
            frame: CGRect(
                x      : 0                ,
                y      : 0                ,
                width  : view.bounds.width,
                height : 80
            )
        )
        
        let spinner = UIActivityIndicatorView()
        
        spinner.style  = .medium
        spinner.center = footerView.center
        spinner.color  = color
        
        spinner.startAnimating()
        footerView.addSubview(spinner)
        
        return footerView
    }
}
