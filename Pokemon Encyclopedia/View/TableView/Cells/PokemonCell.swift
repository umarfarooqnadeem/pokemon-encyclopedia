//
//  PokemonCell.swift
//  Pokemon Encyclopedia
//
//  Created by Umar Farooq on 18/07/2021.
//

import UIKit

class PokemonCell: UITableViewCell {

    /* MARK:- Lazy Properties  */
    lazy var containerView: ShadowNCornerRadiusView = {
        let view = ShadowNCornerRadiusView()
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.AppTheme.white
        
        return view
    }() 
    lazy var pokemonImageView: UIImageView = {
        let imageView = UIImageView()
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "placeholder")
        
        imageView.clipsToBounds      = true
        imageView.layer.cornerRadius = 12.0
        imageView.layer.borderWidth  = 3.0
        imageView.layer.borderColor  = UIColor.AppTheme.black.cgColor
        
        imageView.backgroundColor = UIColor.AppTheme.red
        
        return imageView
    }()
    lazy var labelStack: UIStackView = {
        let stackView = UIStackView()
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis          = .vertical
        stackView.spacing       = 0.0
        stackView.alignment     = .fill
        stackView.distribution  = .fillEqually
        stackView.clipsToBounds = true
        
        stackView.addArrangedSubview(numberLabel)
        stackView.addArrangedSubview(nameLabel)
        stackView.addArrangedSubview(abilityHeadingLabel)
        stackView.addArrangedSubview(abilityOneLabel)
        stackView.addArrangedSubview(abilityTwoLabel)
        
        return stackView
    }()
    lazy var numberLabel: UILabel = {
        let label = UILabel()
        
        label.font      = UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor.AppTheme.blue
        
        return label
    }()
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        
        label.font      = UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor.AppTheme.blue
        
        return label
    }()
    lazy var abilityHeadingLabel: UILabel = {
        let label = UILabel()
        
        label.text      = "Abilities"
        label.font      = .boldSystemFont(ofSize: 16.0)
        label.textColor = UIColor.AppTheme.blue
        
        return label
    }()
    lazy var abilityOneLabel: UILabel = {
        let label = UILabel()
        
        label.font               = UIFont.systemFont(ofSize: 14)
        label.textColor          = UIColor.AppTheme.blue
        label.numberOfLines      = 2
        label.minimumScaleFactor = 0.8
        
        return label
    }()
    lazy var abilityTwoLabel: UILabel = {
        let label = UILabel()
        
        label.font               = UIFont.systemFont(ofSize: 14)
        label.textColor          = UIColor.AppTheme.blue
        label.numberOfLines      = 2
        label.minimumScaleFactor = 0.8
        
        return label
    }()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        pokemonImageView.image = nil
    }
}

/* MARK:- Methods  */
extension PokemonCell {
    func configure(withData data: PokemonModel, atIndex index: Int){
        self.backgroundColor   = UIColor.AppTheme.white
        pokemonImageView.image = nil
        
        if let image = data.detail?.image {
            if let dataUrl = URL(string: image) {
                NetworkManager.shared.loadData(
                    url: dataUrl,
                    withIndicatorOnImageView: pokemonImageView
                ) { data, url, error in
                    MAIN_QUEUE.async { [weak self] in
                        if url == dataUrl {
                            if let data = data {
                                self?.pokemonImageView.image = UIImage(data: data)
                            } else {
                                Helper.debugLogs(anyData: error as Any, andTitle: "ERROR")
                            }
                        } else {
                            self?.pokemonImageView.image = nil
                        }
                    }
                }
            }
        }
    
        if let number = data.detail?.number {
            numberLabel.text = "Number: #\(number)"
        }
        
        if let name = data.name {
            nameLabel.text   = "Name: " + name
        }
        
        if let abilities = data.detail?.abilities {
            if abilities.indices.contains(0) {
                abilityOneLabel.text   = "#1: " + abilities[0]
            }
            if abilities.indices.contains(1) {
                abilityTwoLabel.text   = "#2: " + abilities[1]
            }
        }
        
        selectionStyle = .none
        if !self.subviews.contains(containerView) {
            ///Adding Subviews
            containerView.addSubview(pokemonImageView)
            containerView.addSubview(labelStack)
            addSubview(containerView)
            ///Adding Constraints
            addConstraints()
        }
    }
    
    func addConstraints(){
        let imageWidthHeight = ((SCREEN_HEIGHT * 0.9) / 4.0) - 32.0
        
        var containerViewConstraints = [NSLayoutConstraint]()
        var imageViewConstraints     = [NSLayoutConstraint]()
        var labelStackConstraints    = [NSLayoutConstraint]()
        
        containerViewConstraints = [
            containerView.topAnchor.constraint(equalTo: topAnchor,constant: 8.0)             ,
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8.0)    ,
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8.0) ,
            containerView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8.0)
        ]
        
        imageViewConstraints = [
            pokemonImageView.centerYAnchor.constraint(equalTo: centerYAnchor)                             ,
            pokemonImageView.heightAnchor.constraint(equalToConstant: imageWidthHeight)                   ,
            pokemonImageView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8.0),
            pokemonImageView.widthAnchor.constraint(equalToConstant: imageWidthHeight)
        ]
        
        labelStackConstraints = [
            labelStack.leadingAnchor.constraint(equalTo: pokemonImageView.trailingAnchor, constant: 8.0),
            labelStack.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 12.0)            ,
            labelStack.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -8.0) ,
            labelStack.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -12.0)
        ]
        
        NSLayoutConstraint.activate(containerViewConstraints + imageViewConstraints + labelStackConstraints)
    }
}
