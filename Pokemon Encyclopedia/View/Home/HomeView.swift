//
//  HomeView.swift
//  Pokemon Encyclopedia
//
//  Created by Umar Farooq on 18/07/2021.
//

import UIKit

class HomeView: UIView {
    
    /* MARK:- lazy Properties  */
    lazy var topBar: TopBar = {
        let bar = TopBar(withSearch: true)
        bar.translatesAutoresizingMaskIntoConstraints = false
        return bar
    }()
    lazy var bottomBar : BottomBar = {
        let bar = BottomBar()
        bar.translatesAutoresizingMaskIntoConstraints = false
        return bar
    }()
    lazy var tableView : UITableView = {
        let tv = UITableView()
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.separatorStyle = .none
        tv.showsVerticalScrollIndicator = false
        tv.backgroundColor = UIColor.AppTheme.white
        return tv
    }()
    
    /* MARK:- Initilizers  */
    override init(frame: CGRect) {
        super.init(frame: frame)
        createView()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

/* MARK:- Methods */
extension HomeView {
    func createView(){
        ///self
        backgroundColor = UIColor.AppTheme.white
        
        ///Adding subview
        addSubview(topBar)
        addSubview(bottomBar)
        addSubview(tableView)
        //AddingConstraints
        setupConstraints()
    }
    
    func setupConstraints(){
        var topBarConstraints    = [NSLayoutConstraint]()
        var bottomBarConstraints = [NSLayoutConstraint]()
        var tableViewConstraints = [NSLayoutConstraint]()
        
        topBarConstraints = [
            topBar.leadingAnchor.constraint(equalTo: leadingAnchor)             ,
            topBar.topAnchor.constraint(equalTo: topAnchor)                     ,
            topBar.trailingAnchor.constraint(equalTo: trailingAnchor)           ,
            topBar.heightAnchor.constraint(equalToConstant: SCREEN_HEIGHT * 0.12)
        ]
        
        bottomBarConstraints = [
            bottomBar.leadingAnchor.constraint(equalTo: leadingAnchor)             ,
            bottomBar.bottomAnchor.constraint(equalTo: bottomAnchor)               ,
            bottomBar.trailingAnchor.constraint(equalTo: trailingAnchor)           ,
            bottomBar.heightAnchor.constraint(equalToConstant: SCREEN_HEIGHT * 0.1)
        ]
        
        tableViewConstraints = [
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor)                     ,
            tableView.topAnchor.constraint(equalTo: topBar.bottomAnchor, constant: 4.0)    ,
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor)                   ,
            tableView.bottomAnchor.constraint(equalTo: bottomBar.topAnchor, constant: -4.0)
        ]
        
        NSLayoutConstraint.activate(topBarConstraints + bottomBarConstraints + tableViewConstraints)
    }
}
