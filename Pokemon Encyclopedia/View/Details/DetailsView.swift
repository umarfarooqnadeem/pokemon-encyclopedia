//
//  DetailsView.swift
//  Pokemon Encyclopedia
//
//  Created by Umar Farooq on 20/07/2021.
//

import UIKit

class DetailsView: UIView {
    
    /* MARK:- Properties  */
    public var pokemonDatum: PokemonModel? {
        didSet {
            populateView()
        }
    }
    
    /* MARK:- lazy Properties  */
    lazy var topBar: TopBar = {
        let bar = TopBar(withSearch: false)
        bar.translatesAutoresizingMaskIntoConstraints = false
        return bar
    }()
    lazy var pokemonImageView: UIImageView = {
        let imageView = UIImageView()
                
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        return imageView
    }()
    lazy var horizontalLine: UILabel = {
        let label = UILabel()

        label.backgroundColor = UIColor.AppTheme.blue
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    lazy var stackHr: [CustomStackView] = {
        var hr: [CustomStackView] = []
        for i in 0...5 {
            let stackView = CustomStackView(withAxis: .horizontal, Distribution: .fillEqually)
            
            stackView.backgroundColor = UIColor.AppTheme.blue
            
            let heightAnchor = stackView.heightAnchor.constraint(equalToConstant: 2.0)
            heightAnchor.isActive = true
            
            hr.append(stackView)
        }
        return hr
    }()
    lazy var detailsStack: CustomStackView = {
        let stackView = CustomStackView(withAxis: .vertical, Distribution: .fill, andSpacing: 4.0)
        
        stackView.addArrangedSubview(basicInfoHeaderStack)
        stackView.addArrangedSubview(stackHr[0])
        stackView.addArrangedSubview(numberStack)
        stackView.addArrangedSubview(nameStack)
        stackView.addArrangedSubview(stackHr[1])
        stackView.addArrangedSubview(abilitiesHeaderStack)
        stackView.addArrangedSubview(stackHr[2])
        stackView.addArrangedSubview(abilitiesStacks[0])
        stackView.addArrangedSubview(abilitiesStacks[1])
        stackView.addArrangedSubview(stackHr[3])
        stackView.addArrangedSubview(otherHeaderStack)
        stackView.addArrangedSubview(stackHr[4])
        stackView.addArrangedSubview(heightStack)
        stackView.addArrangedSubview(weightStack)
        stackView.addArrangedSubview(xpStack)
        stackView.addArrangedSubview(stackHr[5])
        
        return stackView
    }()
    ///NUMBER
    lazy var numberStack: CustomStackView = {
        let stackView = CustomStackView(withAxis: .horizontal, Distribution: .fillEqually)
        
        stackView.addArrangedSubview(numberHeadingLabel)
        stackView.addArrangedSubview(numberLabel)
        
        return stackView
    }()
    lazy var numberHeadingLabel: CustomLabel = {
        let label = CustomLabel(withFont: UIFont.boldSystemFont(ofSize: 14.0))
        label.text = "Number:"
        return label
    }()
    lazy var numberLabel: CustomLabel = {
        let label = CustomLabel()
        return label
    }()
    ///NAME
    lazy var nameStack: CustomStackView = {
        let stackView = CustomStackView(withAxis: .horizontal, Distribution: .fillEqually)
        
        stackView.addArrangedSubview(nameHeadingLabel)
        stackView.addArrangedSubview(nameLabel)
        
        return stackView
    }()
    lazy var nameHeadingLabel: CustomLabel = {
        let label = CustomLabel(withFont: UIFont.boldSystemFont(ofSize: 14.0))
        label.text = "Name:"
        return label
    }()
    lazy var nameLabel: CustomLabel = {
        let label = CustomLabel()
        return label
    }()
    ///ATTRIBUTES
    lazy var abilitiesHeadingLabels: [CustomLabel] = {
        var labels: [CustomLabel] = []
        
        for i in 1...2 {
            let label = CustomLabel(withFont: UIFont.boldSystemFont(ofSize: 14.0))
            label.text = "#\(i):"
            labels.append(label)
        }
        
        return labels
    }()
    lazy var abilitiesLabels: [CustomLabel] = {
        var labels: [CustomLabel] = []
        
        for i in 0...1 {
            let label = CustomLabel()
            labels.append(label)
        }
        
        return labels
    }()
    lazy var abilitiesStacks: [CustomStackView] = {
        var stacks: [CustomStackView] = []
        
        for i in 0...1 {
            let stackView = CustomStackView(withAxis: .horizontal, Distribution: .fillEqually)
            
            stackView.addArrangedSubview(abilitiesHeadingLabels[i])
            stackView.addArrangedSubview(abilitiesLabels[i])
            
            stacks.append(stackView)
        }
        
        return stacks
    }()
    ///HEIGHT
    lazy var heightStack: CustomStackView = {
        let stackView = CustomStackView(withAxis: .horizontal, Distribution: .fillEqually)
        
        stackView.addArrangedSubview(heightHeadingLabel)
        stackView.addArrangedSubview(heightLabel)
        
        return stackView
    }()
    lazy var heightHeadingLabel: CustomLabel = {
        let label = CustomLabel(withFont: UIFont.boldSystemFont(ofSize: 14.0))
        label.text = "Height:"
        return label
    }()
    lazy var heightLabel: CustomLabel = {
        let label = CustomLabel()
        return label
    }()
    ///WEIGHT
    lazy var weightStack: CustomStackView = {
        let stackView = CustomStackView(withAxis: .horizontal, Distribution: .fillEqually)
        
        stackView.addArrangedSubview(weightHeadingLabel)
        stackView.addArrangedSubview(weightLabel)
        
        return stackView
    }()
    lazy var weightHeadingLabel: CustomLabel = {
        let label = CustomLabel(withFont: UIFont.boldSystemFont(ofSize: 14.0))
        label.text = "Weight:"
        return label
    }()
    lazy var weightLabel: CustomLabel = {
        let label = CustomLabel()
        return label
    }()
    ///EXPERIIENCE
    lazy var xpStack: CustomStackView = {
        let stackView = CustomStackView(withAxis: .horizontal, Distribution: .fillEqually)
        
        stackView.addArrangedSubview(xpHeadingLabel)
        stackView.addArrangedSubview(xpLabel)
        
        return stackView
    }()
    lazy var xpHeadingLabel: CustomLabel = {
        let label = CustomLabel(withFont: UIFont.boldSystemFont(ofSize: 14.0))
        label.text = "Base Experience:"
        return label
    }()
    lazy var xpLabel: CustomLabel = {
        let label = CustomLabel()
        return label
    }()
    ///HEADERS
    lazy var basicInfoHeaderStack: CustomStackView = {
        let stackView = CustomStackView(withAxis: .horizontal, Distribution: .fillEqually)
        
        stackView.addArrangedSubview(basicInfoHeaderLabel)
        
        return stackView
    }()
    lazy var basicInfoHeaderLabel: CustomLabel = {
        let label  = CustomLabel(withFont: UIFont.boldSystemFont(ofSize: 20))
        label.text = "Basic Info"
        
        return label
    }()
    
    lazy var abilitiesHeaderStack: CustomStackView = {
        let stackView = CustomStackView(withAxis: .horizontal, Distribution: .fillEqually)
    
        stackView.addArrangedSubview(abilitiesHeaderLabel)
        
        return stackView
    }()
    lazy var abilitiesHeaderLabel: CustomLabel = {
        let label  = CustomLabel(withFont: UIFont.boldSystemFont(ofSize: 20))
        label.text = "Abilities"
        
        return label
    }()
    
    lazy var otherHeaderStack: CustomStackView = {
        let stackView = CustomStackView(withAxis: .horizontal, Distribution: .fillEqually)
        
        stackView.addArrangedSubview(otherHeaderLabel)
        
        return stackView
    }()
    lazy var otherHeaderLabel: CustomLabel = {
        let label  = CustomLabel(withFont: UIFont.boldSystemFont(ofSize: 20))
        label.text = "Other Info"
        
        return label
    }()

    /* MARK:- Initilizers  */
    override init(frame: CGRect) {
        super.init(frame: frame)
        createView()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

/* MARK:- Methods */
extension DetailsView {
    func populateView(){
        if let image = pokemonDatum?.detail?.image {
            if let dataUrl = URL(string: image) {
                NetworkManager.shared.loadData(
                    url: dataUrl,
                    withIndicatorOnImageView: pokemonImageView
                ) { data, url, error in
                    MAIN_QUEUE.async { [weak self] in
                        if url == dataUrl {
                            if let data = data {
                                self?.pokemonImageView.image = UIImage(data: data)
                            } else {
                                Helper.debugLogs(anyData: error as Any, andTitle: "ERROR")
                            }
                        } else {
                            self?.pokemonImageView.image = nil
                        }
                    }
                }
            }
        }
        
        if let number = pokemonDatum?.detail?.number {
            numberLabel.text = "\(number)"
        }
        
        if let name = pokemonDatum?.name {
            nameLabel.text   = name
        }
        
        if let abilities = pokemonDatum?.detail?.abilities {
            for i in 0...1 {
                abilitiesLabels[i].text = abilities[i]
            }
        }
        
        if let height = pokemonDatum?.detail?.height {
            heightLabel.text = "\(height)"
        }
        if let weight = pokemonDatum?.detail?.weight {
            weightLabel.text = "\(weight)"
        }
        if let baseXP = pokemonDatum?.detail?.baseXP {
            xpLabel.text = "\(baseXP)"
        }
    }
    func createView(){
        ///self
        backgroundColor = UIColor.AppTheme.white
        
        ///Adding subview
        addSubview(topBar)
        addSubview(pokemonImageView)
        addSubview(horizontalLine)
        addSubview(detailsStack)
        //AddingConstraints
        setupConstraints()
    }
    
    func setupConstraints(){
        var topBarConstraints          = [NSLayoutConstraint]()
        var imageViewConstraints       = [NSLayoutConstraint]()
        var horizontalLineConstraints  = [NSLayoutConstraint]()
        var detailStackViewConstraints = [NSLayoutConstraint]()
        
        
        topBarConstraints = [
            topBar.leadingAnchor.constraint(equalTo: leadingAnchor)             ,
            topBar.topAnchor.constraint(equalTo: topAnchor)                     ,
            topBar.trailingAnchor.constraint(equalTo: trailingAnchor)           ,
            topBar.heightAnchor.constraint(equalToConstant: SCREEN_HEIGHT * 0.12)
        ]
        
        imageViewConstraints = [
            pokemonImageView.leadingAnchor.constraint(equalTo: leadingAnchor)            ,
            pokemonImageView.topAnchor.constraint(equalTo: topBar.bottomAnchor)          ,
            pokemonImageView.trailingAnchor.constraint(equalTo: trailingAnchor)          ,
            pokemonImageView.heightAnchor.constraint(equalToConstant: SCREEN_HEIGHT * 0.3)
        ]
        
        horizontalLineConstraints = [
            horizontalLine.leadingAnchor.constraint(equalTo: leadingAnchor)            ,
            horizontalLine.topAnchor.constraint(equalTo: pokemonImageView.bottomAnchor),
            horizontalLine.trailingAnchor.constraint(equalTo: trailingAnchor)          ,
            horizontalLine.heightAnchor.constraint(equalToConstant: 2.0)
        ]
        
        detailStackViewConstraints = [
            detailsStack.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16)   ,
            detailsStack.topAnchor.constraint(equalTo: horizontalLine.bottomAnchor)       ,
            detailsStack.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
        ]
        
        NSLayoutConstraint.activate(topBarConstraints + imageViewConstraints + horizontalLineConstraints + detailStackViewConstraints)
    }
}
