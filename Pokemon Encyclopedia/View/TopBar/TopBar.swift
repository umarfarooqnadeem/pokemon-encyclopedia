//
//  TopBar.swift
//  Pokemon Encyclopedia
//
//  Created by Umar Farooq on 18/07/2021.
//

import UIKit

class TopBar: UIView {

    /* MARK:- Properties  */
    private var shouldAddSearch: Bool
    
    /* MARK:- Lazy Properties  */
    //View
    lazy var containerView: UIView = {
        let view = UIView()
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    ///SearchBar
    lazy var searchBar: UISearchBar = {
        let bar = UISearchBar()
        
        bar.translatesAutoresizingMaskIntoConstraints = false
        bar.searchTextField.backgroundColor = .white
        bar.searchBarStyle = .minimal
        bar.showsCancelButton = true
        bar.delegate = self
        
        return bar
    }()
    ///Button
    lazy var searchButton: UIButton = {
        let button = UIButton()
        
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "search-ico"), for: .normal)
        button.addTarget(self, action: #selector(didTapSearch(_:)), for: .touchUpInside)
        
        return button
    }()
    lazy var backButton: UIButton = {
        let button = UIButton()
        
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "back-button"), for: .normal)
        button.addTarget(self, action: #selector(didTapBack(_:)), for: .touchUpInside)
        
        return button
    }()
    ///Image VIew
    lazy var headingImageView: UIImageView = {
        let imageView = UIImageView()
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "heading")
        
        return imageView
    }()
    
    /* MARK:- Properties  */
    /// Private
    private var searchBarWidthAnchor: NSLayoutConstraint?
    
    /* MARK:- Initilizers  */
    init(withSearch search: Bool) {
        shouldAddSearch = search
        super.init(frame: .zero)
        createView()
    }
    
    override init(frame: CGRect) {
        shouldAddSearch = true
        super.init(frame: frame)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

/* MARK:- Methods */
extension TopBar {
    func createView(){
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
        ///Self
        backgroundColor = UIColor.AppTheme.red
        
        layer.shadowColor   = UIColor.black.cgColor
        layer.shadowOffset  = CGSize(width: 0.0, height: 2.0)
        layer.shadowRadius  = 2.0
        layer.shadowOpacity = 0.5
        
        ///Adding subviews
        if shouldAddSearch {
            containerView.addSubview(searchButton)
            containerView.addSubview(searchBar)
        } else {
            containerView.addSubview(backButton)
        }
        containerView.addSubview(headingImageView)
        addSubview(containerView)
        
        ///AddingConstraints
        addConstraints()
    }
    
    func addConstraints(){
        var containerViewConstraints    = [NSLayoutConstraint]()
        var headingImageViewConstraints = [NSLayoutConstraint]()
        
        containerViewConstraints = [
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor)              ,
            containerView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor)  ,
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor)            ,
            containerView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
        
        headingImageViewConstraints = [
            headingImageView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: -4.0),
            headingImageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor)              ,
            headingImageView.widthAnchor.constraint(equalToConstant: SCREEN_WIDTH * 0.6)                 ,
            headingImageView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -4.0)
        ]
        
        if shouldAddSearch {
            var searchBarConstraints    = [NSLayoutConstraint]()
            var searchButtonConstraints = [NSLayoutConstraint]()
            
            searchBarConstraints = [
                searchBar.centerYAnchor.constraint(equalTo: containerView.centerYAnchor)              ,
                searchBar.centerXAnchor.constraint(equalTo: containerView.centerXAnchor)              ,
                searchBar.heightAnchor.constraint(equalTo: containerView.heightAnchor, constant: -10.0)
            ]
            
            searchButtonConstraints = [
                searchButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -12.0),
                searchButton.centerYAnchor.constraint(equalTo: containerView.centerYAnchor)                   ,
                searchButton.widthAnchor.constraint(equalToConstant: SCREEN_WIDTH * 0.06)                     ,
                searchButton.heightAnchor.constraint(equalToConstant: SCREEN_WIDTH * 0.06)
            ]
            
            NSLayoutConstraint.activate(
                containerViewConstraints + searchBarConstraints + headingImageViewConstraints + searchButtonConstraints
            )
        } else {
            var backButtonConstraints = [NSLayoutConstraint]()
            
            backButtonConstraints = [
                backButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 12.0),
                backButton.centerYAnchor.constraint(equalTo: containerView.centerYAnchor)                ,
                backButton.widthAnchor.constraint(equalToConstant: SCREEN_WIDTH * 0.1)                   ,
                backButton.heightAnchor.constraint(equalToConstant: SCREEN_WIDTH * 0.1)
            ]
            
            NSLayoutConstraint.activate(
                containerViewConstraints + backButtonConstraints + headingImageViewConstraints
            )
        }
        
    }
}


/* MARK:- Obcj Interface */
extension TopBar {
    @objc func didTapSearch(_ sender: UIButton){
        searchButton.isHidden     = true
        headingImageView.isHidden = true
        
        if let searchBarWidthAnchor = searchBarWidthAnchor {
            searchBarWidthAnchor.isActive = true
        } else {
            searchBarWidthAnchor = searchBar.widthAnchor.constraint(equalToConstant: SCREEN_WIDTH * 0.9)
            searchBarWidthAnchor!.isActive = true
        }
        
        searchBar.searchTextField.becomeFirstResponder()
    }
    
    @objc func didTapBack(_ sender: UIButton){
        parentViewController?.navigationController?.popViewController(animated: true)
    }
}

/* MARK:- SearchBar */

///DELEGATE
extension TopBar: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        
        searchButton.isHidden     = false
        headingImageView.isHidden = false
        
        if let searchBarWidthAnchor = searchBarWidthAnchor {
            searchBarWidthAnchor.isActive = false
        }
        if let homeVC = parentViewController as? HomeVC {
            homeVC.isSearching  = false
            homeVC.filteredData = homeVC.originalData
            homeVC.homeView.tableView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if let homeVC = parentViewController as? HomeVC {
            homeVC.isSearching = true
            let searchText: String! = searchBar.text
            
            homeVC.filteredData = Helper.search(text: searchText, fromData: homeVC.originalData)
            homeVC.homeView.tableView.reloadData()
        }
    }
}
