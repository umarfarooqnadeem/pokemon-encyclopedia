//
//  BottomBar.swift
//  Pokemon Encyclopedia
//
//  Created by Umar Farooq on 18/07/2021.
//

import UIKit

class BottomBar: UIView {
    
    /* MARK:- Lazy Properties  */
    ///View
    lazy var containerView: UIView = {
        let view = UIView()
        
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    ///Buttons
    lazy var sortNumerically: UIButton = {
        let button = UIButton()
        
        if SORT_SELECTED == SortTypes.numerically.rawValue {
            button.setImage(UIImage(named: "numerical-selected"), for: .normal)
        } else {
            button.setImage(UIImage(named: "numerical-unselected"), for: .normal)
        }
        
        button.addTarget(self, action: #selector(didTapSortNumerically(_:)), for: .touchUpInside)
        
        return button
    }()
    lazy var sortAlphabetically: UIButton = {
        let button = UIButton()
        
        if SORT_SELECTED == SortTypes.alphabetically.rawValue {
            button.setImage(UIImage(named: "alphabetiacally-selected"), for: .normal)
        } else {
            button.setImage(UIImage(named: "alphabetiacally-unselected"), for: .normal)
        }
        
        button.addTarget(self, action: #selector(didTapSortAlphabetically(_:)), for: .touchUpInside)
        
        return button
    }()
    ///Stack
    lazy var sortingStack: UIStackView = {
        let stack = UIStackView()
        
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis         = .horizontal
        stack.spacing      = 40
        stack.alignment    = .fill
        stack.distribution = .fillEqually
        
        return stack
    }()
    
    /* MARK:- Initilizers  */
    override init(frame: CGRect) {
        super.init(frame: frame)
        createView()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

/* MARK:- Methods */
extension BottomBar {
    func createView(){
        ///Self
        backgroundColor = UIColor.AppTheme.red
        
        layer.shadowColor   = UIColor.AppTheme.black.cgColor
        layer.shadowOffset  = CGSize(width: 0.0, height: -2.0)
        layer.shadowRadius  = 2.0
        layer.shadowOpacity = 0.5
        
        ///StackView
        sortingStack.addArrangedSubview(sortAlphabetically)
        sortingStack.addArrangedSubview(sortNumerically)
        
        ///Adding subviews
        containerView.addSubview(sortingStack)
        addSubview(containerView)
        
        //AddingConstraints
        addConstraints()
    }
    
    func addConstraints(){
        var containerViewConstraints = [NSLayoutConstraint]()
        var stackViewConstraints     = [NSLayoutConstraint]()
        
        containerViewConstraints = [
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor)                  ,
            containerView.topAnchor.constraint(equalTo: topAnchor)                          ,
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor)                ,
            containerView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
        ]
        
        stackViewConstraints = [
            sortingStack.widthAnchor.constraint(equalToConstant: SCREEN_WIDTH * 0.6)                      ,
            sortingStack.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 8.0)            ,
            sortingStack.centerXAnchor.constraint(equalTo: containerView.centerXAnchor)                   ,
            sortingStack.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: 8.0)
        ]
        
        NSLayoutConstraint.activate(containerViewConstraints + stackViewConstraints)
    }
}

/* MARK:- Objc Interface */
extension BottomBar {
    @objc func didTapSortNumerically(_ sender: UIButton){
        sortNumerically.setImage(UIImage(named: "numerical-selected"), for: .normal)
        sortAlphabetically.setImage(UIImage(named: "alphabetiacally-unselected"), for: .normal)
        
        if let homeVC = parentViewController as? HomeVC,
           SORT_SELECTED != SortTypes.numerically.rawValue {
            Helper.sort(modelData: homeVC.filteredData, byType: .numerically) { result in
                SORT_SELECTED = "numerically"
                
                homeVC.originalData = result
                homeVC.filteredData = result
                homeVC.homeView.tableView.reloadData()
            }
        }
    }
    
    @objc func didTapSortAlphabetically(_ sender: UIButton){
        sortNumerically.setImage(UIImage(named: "numerical-unselected"), for: .normal)
        sortAlphabetically.setImage(UIImage(named: "alphabetiacally-selected"), for: .normal)
        
        if let homeVC = parentViewController as? HomeVC,
           SORT_SELECTED != SortTypes.alphabetically.rawValue {
            Helper.sort(modelData: homeVC.filteredData, byType: .alphabetically) { result in
                SORT_SELECTED = "aplhabetically"
                
                homeVC.originalData = result
                homeVC.filteredData = result
                homeVC.homeView.tableView.reloadData()
            }
        }
    }
}
