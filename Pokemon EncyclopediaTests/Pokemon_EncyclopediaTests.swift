//
//  Pokemon_EncyclopediaTests.swift
//  Pokemon EncyclopediaTests
//
//  Created by Umar Farooq on 18/07/2021.
//

import XCTest
@testable import Pokemon_Encyclopedia

class Pokemon_EncyclopediaTests: XCTestCase {
    /* MARK:- Properties */
    ///Private
    private let group       : DispatchGroup   = DispatchGroup()
    private var tempData    : [PokemonModel?] = []
    ///Public
    public var originalData : [PokemonModel]  = []
    
    func testNumberSearch(){
        fetchPokemon { [self] isCompleted in
            if isCompleted {
                let searchText = "1"
                
                let result = Helper.search(text: searchText, fromData: self.originalData)
                XCTAssertEqual(result.count, 1)
            }
        }
    }
    
    func testNameSearch(){
        fetchPokemon { [self] isCompleted in
            if isCompleted {
                let searchText = "bulbasaur"
                
                let result = Helper.search(text: searchText, fromData: self.originalData)
                XCTAssertEqual(result.count, 1)
            }
        }
    }
    
    func testWrongSearch(){
        fetchPokemon { [self] isCompleted in
            if isCompleted {
                let searchText = "@"
                
                let result = Helper.search(text: searchText, fromData: self.originalData)
                XCTAssertEqual(result.count, 0)
            }
        }
    }
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func fetchPokemon(completion: @escaping (_ completed: Bool) -> ()){
        NetworkManager.shared.makeRequest(
            toUrl: "https://pokeapi.co/api/v2/pokemon/?offset=\(0)&limit=\(10)"
        ) { [weak self] response in
            guard let self = self else { return }
            
            switch response {
            case .success(let result):
                
                if let data = result["results"] as? [[String: Any]] {
                    for datum in data {
                        if let url = datum["url"] as? String {
                            self.group.enter()
                            self.fetchPokemonDetails(fromUrl: url)
                        }
                    }
                    
                    self.group.notify(queue: .main) { [weak self] in
                        guard let self = self else {return}
                        if self.tempData.count == 10 {
                            for datum in self.tempData {
                                if let pokemonDatum = datum {
                                    self.originalData.append(pokemonDatum)
                                }
                            }
                            
                            self.tempData = []
                            completion(true)
                        }
                    }
                }
                
                Helper.debugLogs(anyData: self.originalData, andTitle: "SUCCESS")
            case .badCode(let code):
                Helper.debugLogs(anyData: code, andTitle: "BAD CODE")
            case .invalid(let message):
                Helper.debugLogs(anyData: message, andTitle: "INVALID")
            case .timeout(let message):
                Helper.debugLogs(anyData: message, andTitle: "TIME OUT")
            case .faliure(let error):
                Helper.debugLogs(anyData: error, andTitle: "FALIURE")
            }
        }
    }
    
    func fetchPokemonDetails(fromUrl url: String) {
        NetworkManager.shared.makeRequest(
            toUrl: url
        ) { [weak self] response in
            guard let self = self else { return }
            
            switch response {
            case .success(let result):
                let datum = PokemonModel(data: result)
                self.tempData.append(datum)
                
                Helper.debugLogs(anyData: result, andTitle: "SUCCESS")
            case .badCode(let code):
                self.group.leave()
                self.tempData.append(nil)
                Helper.debugLogs(anyData: code, andTitle: "BAD CODE")
            case .invalid(let message):
                self.group.leave()
                self.tempData.append(nil)
                Helper.debugLogs(anyData: message, andTitle: "INVALID")
            case .timeout(let message):
                self.group.leave()
                self.tempData.append(nil)
                Helper.debugLogs(anyData: message, andTitle: "TIME OUT")
            case .faliure(let error):
                self.group.leave()
                self.tempData.append(nil)
                Helper.debugLogs(anyData: error, andTitle: "FALIURE")
            }
            self.group.leave()
        }
        
    }
    
    
}
